# frozen_string_literal: true

require_relative "elastic_caribou/version"

module ElasticCaribou
  class Error < StandardError; end
  # Your code goes here...
end
